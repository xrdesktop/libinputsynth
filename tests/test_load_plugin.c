/*
 * libinputsynth
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <inputsynth.h>

int
main ()
{
  InputSynth *input = INPUT_SYNTH (input_synth_new (INPUTSYNTH_BACKEND_XDO));
  g_assert (input != NULL);
  GString *name = input_synth_get_backend_name (input);
  g_print ("Input Synth Backend: %s\n", name->str);
  g_assert (name != NULL);
  g_string_free (name, FALSE);
  return 0;
}

