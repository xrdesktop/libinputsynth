/*
 * LibInputSynth
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef __INPUT_SYNTH_XI2_H__
#define __INPUT_SYNTH_XI2_H__

#include <glib-object.h>
#include "inputsynth.h"
#include <gmodule.h>

// this is the name we give to the second pointer
#define INPUT_SYNTH_MASTER_CREATE_NAME "InputSynthPointer"
// xinput appends the " pointer" suffix to the master pointer
#define INPUT_SYNTH_MASTER_NAME "InputSynthPointer pointer"
// an "XTEST" slave pointer will be automatically created for us when creating a
// second master pointer.
// xinput 2 will also append the suffix " pointer" to the slave pointer
#define INPUT_SYNTH_SLAVE_NAME "InputSynthPointer XTEST pointer"

#define INPUT_SYNTH_KEYBOARD_SLAVE_NAME "InputSynthPointer XTEST keyboard"

G_BEGIN_DECLS

#define INPUT_TYPE_SYNTH_XI2 input_synth_xi2_get_type()
G_DECLARE_FINAL_TYPE (InputSynthXi2, input_synth_xi2, INPUT, SYNTH_XI2, InputSynth)

struct _InputSynthXi2Class
{
  InputSynthXi2Class parent;
};

struct _InputSynthXi2;

G_MODULE_EXPORT InputSynthXi2 *input_synth_xi2_new (void);

G_END_DECLS

#endif /* __INPUT_SYNTH_XI2_H__ */
