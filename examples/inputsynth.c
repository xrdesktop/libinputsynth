/*
 * LibInputSynth
 * Copyright 2018 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * SPDX-License-Identifier: MIT
 *
 * This example
 * - moves the cursor from (0,0) to (100,1000)
 * - clicks on (500,500)
 * - writes "hello"
 */

#define GETTEXT_PACKAGE "gtk30"
#include <glib.h>
#include <glib/gi18n-lib.h>

#include <inputsynth.h>
#include <unistd.h>
#include <X11/keysym.h>

#define MSEC_TO_NSEC 1000000

static inline void
_nanosleep(int64_t nsec)
{
  struct timespec spec;
  spec.tv_sec = (nsec / 1000000000l);
  spec.tv_nsec = (nsec % 1000000000l);
  nanosleep(&spec, NULL);
}

static gboolean
_run_exmaple (InputSynth *input)
{
  GString *name = input_synth_get_backend_name (input);
  g_print ("Using %s\n", name->str);

  char * strings[] = {
    "hello",
  "أحمر😊",
  "绿色的🤣",
  "नीला🙏"
  };

  g_print ("keyboard input in 1 second...\n");
  _nanosleep (1000 * MSEC_TO_NSEC);

  for (unsigned long i = 0; i < sizeof (strings) / sizeof (strings[0]); i++)
    {
      input_synth_characters (input, strings[i]);
      _nanosleep (100 * MSEC_TO_NSEC);

        for (glong j = 0; j < g_utf8_strlen (strings[i], -1); j++)
          {
            input_synth_keyvals (input, (guint[]){ XK_BackSpace }, 1);
            _nanosleep (100 * MSEC_TO_NSEC);
          }
    }

  input_synth_characters (input, "helloo!");
  _nanosleep (100 * MSEC_TO_NSEC);
  input_synth_keyvals (input, (guint[]){ XK_Left }, 1);
  _nanosleep (100 * MSEC_TO_NSEC);
  input_synth_keyvals (input, (guint[]){ XK_Control_L, XK_h }, 2);
  _nanosleep (100 * MSEC_TO_NSEC);
  input_synth_characters (input, ", world");

  g_print ("Moving cursor...\n");
  for (int i = 0; i < 500; i += 5)
    {
      int y = i * 2;
      int x = i * 2;
      input_synth_move_cursor (input, x, y);
      if (i == 250)
        {
          /* Right click press and release */
          input_synth_click (input, x, y, 3, TRUE);
          _nanosleep (10 * MSEC_TO_NSEC);
          input_synth_click (input, x, y, 3, FALSE);
        }
        _nanosleep (10 * MSEC_TO_NSEC);
    }

  int char_x = 500;
  int char_y = 500;


  g_print ("Right click at %dx%d...\n", char_x, char_y);

  input_synth_move_cursor (input, char_x, char_y);

  input_synth_click (input, char_x, char_y, 3, TRUE);
  _nanosleep (500 * MSEC_TO_NSEC);
  input_synth_click (input, char_x, char_y, 3, FALSE);

  _nanosleep (500 * MSEC_TO_NSEC);

  input_synth_click (input, char_x, char_y, 3, TRUE);
  _nanosleep (500 * MSEC_TO_NSEC);
  input_synth_click (input, char_x, char_y, 3, FALSE);

  return TRUE;
}

int
main ()
{
  InputSynthBackend backends[] = {
    INPUTSYNTH_BACKEND_XDO,
    INPUTSYNTH_BACKEND_XI2,
  };


  for (unsigned long i = 0; i  < sizeof (backends) / sizeof (backends[0]); i++)
  {
    InputSynth *input = input_synth_new (backends[i]);
    if (!input)
      {
        g_print ("Skipping backend %lu (%d)\n", i, backends[i]);
        continue;
      }

    _run_exmaple (input);
    g_object_unref (input);
  }
  
  return 0;
}
